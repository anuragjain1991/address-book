from settings import *


class Person(Model):
    first_name = CharField()
    last_name = CharField()

    class Meta:
        database = db # This model uses the "addressbook.db" database.
        indexes = (
            # create a unique on first_name and last_name
            (('first_name', 'last_name'), True),
        )

    def addEmail(self, email):
        try:
            with db.transaction():
                EmailAddress.create(person=self, email=email)
                return True
        except IntegrityError:
            return False

    def addPhoneNumber(self, phoneNumber):
        try:
            with db.transaction():
                PhoneNumber.create(person=self, phoneNumber=phoneNumber)
                return True
        except IntegrityError:
            return False

    def addStreetAddress(self, addressLine, city, areaCode):
        StreetAddress.create(person=self, addressLine=addressLine, city=city, areaCode=areaCode)
        return True

    def addToGroup(self, groupName):
        group, created = Group.get_or_create(name = groupName)
        try:
            with db.transaction():
                GroupMapping.create(person=self, group=group)
                return True
        except IntegrityError:
            return False

    def getAllGroups(self):
        return Group.select().join(GroupMapping).join(Person).where(Person.id == self.id)


class PhoneNumber(Model):
    person = ForeignKeyField(Person, related_name='phoneNumbers')
    phoneNumber = BigIntegerField(unique=True)

    class Meta:
        database = db # This model uses the "addressbook.db" database.


class EmailAddress(Model):
    person = ForeignKeyField(Person, related_name='emailAddresses')
    email = CharField(unique=True)

    class Meta:
        database = db # This model uses the "addressbook.db" database.


class StreetAddress(Model):
    person = ForeignKeyField(Person, related_name='streetAddresses')
    addressLine = CharField()
    city = CharField()
    areaCode = IntegerField()

    class Meta:
        database = db # This model uses the "addressbook.db" database.


class Group(Model):
    name = CharField(unique=True)

    class Meta:
        database = db # This model uses the "addressbook.db" database.

    def getAllPersons(self):
        return Person.select().join(GroupMapping).join(Group).where(Group.id == self.id)


# add multi constraint
class GroupMapping(Model):
    person = ForeignKeyField(Person, related_name='groups')
    group = ForeignKeyField(Group, related_name='persons')

    class Meta:
        database = db # This model uses the "addressbook.db" database.
        indexes = (
            # create a unique on person and group
            (('person', 'group'), True),
        )


