from settings import *
from models import *

def create_tables():
    connectDB()
    db.create_tables([Person, PhoneNumber, EmailAddress, StreetAddress, Group, GroupMapping], True)

def connectDB():
    db.connect()

def closeDB():
    db.close()
