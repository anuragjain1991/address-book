from unittest import TestCase
from playhouse.test_utils import test_database

from addressbook.settings import *
from addressbook.models import *

import addressbook.views as views


class TestData(TestCase):

    def testAddPerson(self):
        with test_database(test_db, (Person,)):
            # This data will be created in `test_db`
            views.addPerson('anurag','jain')
            retrievedPerson = Person.get(Person.first_name=='anurag',Person.last_name=='jain')

            self.assertEqual(retrievedPerson.first_name,'anurag')
            self.assertEqual(retrievedPerson.last_name,'jain')


    def testAddGroup(self):
        with test_database(test_db, (Group,)):
            # This data will be created in `test_db`
            views.addGroup('friends')
            retrievedGroup = Group.get(Group.name=='friends')

            self.assertEqual(retrievedGroup.name,'friends')


    def testFindPersonsByEmail(self):
        with test_database(test_db, (Group,Person, EmailAddress)):
            person = Person.create(first_name='anurag', last_name='jain')
            EmailAddress.create(person=person, email='anuragjain1991@gmail.com')

            # there can only be one entry with same email
            foundPerson = views.findPersonsByEmail('anuragjain1991@gmail.com')[0]
            self.assertEqual(person.id, foundPerson.id)

            # with email prefix, there could be multiple matches
            foundPersons = views.findPersonsByEmail('anu')
            foundPersonIds = []
            for foundPerson in foundPersons:
                foundPersonIds.append(foundPerson.id)
            
            self.assertIn(person.id, foundPersonIds)


    def testFindPersonsByName(self):
        with test_database(test_db, (Group,Person)):
            person = Person.create(first_name='anurag', last_name='jain')
            
            foundPersons = views.findPersonsByName(first_name ='anurag')
            foundPersonIds = []
            for foundPerson in foundPersons:
                foundPersonIds.append(foundPerson.id)
            
            self.assertIn(person.id, foundPersonIds)

            foundPersons = views.findPersonsByName(first_name ='anurag', last_name='jain')
            foundPersonIds = []
            for foundPerson in foundPersons:
                foundPersonIds.append(foundPerson.id)
            
            self.assertIn(person.id, foundPersonIds)

            foundPersons = views.findPersonsByName(last_name='jain')
            foundPersonIds = []
            for foundPerson in foundPersons:
                foundPersonIds.append(foundPerson.id)
            
            self.assertIn(person.id, foundPersonIds)


    def testAddPersonToGroup(self):
        with test_database(test_db, (Group, Person, GroupMapping)):
            person = Person.create(first_name='anurag', last_name='jain')
            group = Group.create(name='friends')
            
            views.addPersonToGroup(person, 'friends')

            count = GroupMapping.select().where(GroupMapping.person == person, GroupMapping.group == group).count()
            self.assertEqual(count,1)


    def testGetMembersOfGroup(self):
        with test_database(test_db, (Group,Person,GroupMapping)):
            person = Person.create(first_name='anurag', last_name='jain')
            group = Group.create(name='friends')
            GroupMapping.create(group=group, person=person)

            foundPersons = views.getMembersOfGroup('friends')
            foundPersonIds = []
            for foundPerson in foundPersons:
                foundPersonIds.append(foundPerson.id)
            
            self.assertIn(person.id, foundPersonIds)


    def testGetGroupsOfPerson(self):
        with test_database(test_db, (Group,Person,GroupMapping)):
            person = Person.create(first_name='anurag', last_name='jain')
            group = Group.create(name='friends')
            GroupMapping.create(group=group, person=person)

            foundGroups = views.getGroupsOfPerson(person)  
            foundGroupIds = []
            for foundGroup in foundGroups:
                foundGroupIds.append(foundGroup.id)
            
            self.assertIn(group.id, foundGroupIds)


