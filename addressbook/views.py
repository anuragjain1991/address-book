from models import *


def initialize():
	from app import create_tables
	create_tables()


def addPerson(first_name, last_name):
	try:
		with db.transaction():
			Person.create(first_name=first_name, last_name=last_name)
			return True
	except IntegrityError:
		return False
 

def addGroup(name):
	try:
		with db.transaction():
			Group.create(name=name)
			return True
	except IntegrityError:
		return False


def findPersonsByName(first_name=None, last_name=None):
	if first_name is None and last_name is not None:
		return list(Person.select().where(Person.last_name == last_name))
	elif first_name is not None and last_name is None:
		return list(Person.select().where(Person.first_name == first_name))
	elif first_name is not None and last_name is not None:
		return list(Person.select().where(Person.first_name == first_name, Person.last_name == last_name))
	else:
		return None


def findPersonsByEmail(email, isPrefix=True):
	if isPrefix:
		return list(Person.select().join(EmailAddress).where(EmailAddress.email.startswith(email)))
	else:
		# for the case when the parameter can be any substring, use contains instead of startswith
		return list(Person.select().join(EmailAddress).where(EmailAddress.email.contains(email)))
		

def addPersonToGroup(person, groupName):
	return person.addToGroup(groupName)


def addEmailToPerson(person, email):
	return person.addEmail(email)


def addPhoneNumberToPerson(person, phoneNumber):
	return person.addPhoneNumber(phoneNumber)


def getMembersOfGroup(groupName):
	try:
		group = Group.get(Group.name ==groupName)
		return list(group.getAllPersons())
	except:
		return "group not found"


def getGroupsOfPerson(person):
	return list(person.getAllGroups())


