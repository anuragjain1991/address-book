from setuptools import setup

setup(name='addressbook',
      version='0.1',
      description='A basic address book',
      url='https://bitbucket.org/anuragjain1991/address-book/',
      author='Anurag Jain',
      author_email='anuragjain1991@gmail.com',
      license='MIT',
      packages=['addressbook'],
      install_requires=[
          'peewee',
      ],
      zip_safe=False)